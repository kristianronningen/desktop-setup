# desktop-setup

Scripts and stuff I use to set up my own desktop (Ubuntu 18.10 with Xubuntu
desktop (Xfce)). Use at your own risk.

To start, run this:

```
sudo apt-get -y install git
mkdir -p ~/repositories/private
cd ~/repositories/private
git clone https://gitlab.com/kristianronningen/desktop-setup.git
cd desktop-setup
./desktop-setup.sh
```

# Initial installation

I encrypt as much of the disk as I can. Generally I follow this guide:

<https://help.ubuntu.com/community/Full_Disk_Encryption_Howto_2019>

# Homedir in git

Note to self: How to set up hcgit

```
$ cat .bash_aliases 
alias hcgit='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
```

```
git clone --bare git@gitlab.server.here:path-to/desktop-config.git $HOME/.cfg
hcgit config --local status.showUntrackedFiles no
hcgit checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} mv {} .config-backup/ # To move any colliding files out of the way
hcgit checkout

# First time when pushing, set upstream:
#hcgit push --set-upstream origin master
```
