#!/bin/bash

# This script and everything that follows expects:
# * Ubuntu 18.10
# * Xfce window manager

# Install virtualenv to create a python virtual environment for running
# ansible.
sudo apt-get install python3-venv

# Create a python virtualenv if it doesn't already exist.
if [ ! -d "${HOME}/venv-ansible" ]; then
    python3 -m venv ~/venv-ansible
fi

# Activate our virtualenv
source ${HOME}/venv-ansible/bin/activate

# If ansible is not installed, install it.
pip list 2>&1 | egrep -q "^ansible\s"
if [ ${?} -ne 0 ]; then
    pip install ansible dnspython
fi

# If the desktop-vars.yml file doesn't exist, ask for values and create it
if [ ! -f desktop-vars.yml ]; then
    # Ask about username and repo etc to put into the desktop-vars.yml file
    read -p "What is your full name (used for git config)? " my_name
    read -p "What is your email address (used for git config)? " my_email
    read -p "What is your username? " my_user
    read -p "What is your home-in-git repository url? " my_repo

    # Write the desktop-vars.yml file
    echo "---" > desktop-vars.yml
    echo "my_user: \"${my_user}\"" >> desktop-vars.yml
    echo "my_email: \"${my_email}\"" >> desktop-vars.yml
    echo "my_name: \"${my_name}\"" >> desktop-vars.yml
    echo "my_repo: \"${my_repo}\"" >> desktop-vars.yml
fi

# Run the desktop-setup playbook
ansible-playbook desktop-setup.yml -D
