---
# TODO?
# sysctl dev.i915.perf_stream_paranoid=0 # For Lutris
#
- name: "Configure desktop"
  hosts: localhost
  connection: local
  become: yes

  tasks:
    - name: "Include variables from separate file"
      include_vars: "desktop-vars.yml"

    - name: "Make sure required variables are set"
      assert:
        that:
          - my_user | length > 0
          - my_email | length > 0
          - my_name | length > 0
          - my_repo | length > 0
        fail_msg: "One or more required variables are not set"

    - name: "Add package repository key for Google"
      apt_key:
        url: "https://dl.google.com/linux/linux_signing_key.pub"
        state: present

    - name: "Add package repository source for Google"
      apt_repository:
        repo: "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main"
        filename: "google-chrome"
        state: present

    - name: "Add package repository key for Signal"
      apt_key:
        url: "https://updates.signal.org/desktop/apt/keys.asc"
        state: present

    - name: "Add package repository source for Signal"
      apt_repository:
        repo: "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main"
        filename: "signal"
        state: present

    - name: "Add package repository key for VS Code"
      apt_key:
        url: "https://packages.microsoft.com/keys/microsoft.asc"
        state: present

    - name: "Add package repository source for VS Code"
      apt_repository:
        repo: "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
        filename: "vscode"
        state: present

    - name: "Install some packages"
      apt:
        name:
          - ack
          - apt-transport-https
          - conky
          - dnsutils
          - evince
          - gnupg
          - ipcalc
          - mplayer
          - ncftp
          - nmap
          - ntp
          - php-cli
          - pwgen
          - rdiff-backup
          - traceroute
          - terminator
          - telnet
          - thunderbird
          - tmux
          - tree
          - vim
          - whois
          - google-chrome-stable
          - signal-desktop
          - code
          - fonts-firacode
          - libpango1.0-0
          - python3-gpg
          - curl
          - mate-calc
          - bluez-tools
          - blueman
          - plocate
          - bind9-dnsutils
          - jq
          - ssmtp
          - rofi
          - wmctrl
          - xdotool
        state: present

    - name: "Make vim the default editor"
      alternatives:
        name: "editor"
        path: "/usr/bin/vim.basic"

    - name: "Install system vimrc.local (Debian)"
      template:
        src: "etc/vim/vimrc.local.j2"
        dest: "/etc/vim/vimrc.local"

    - name: "Create bashrc dir"
      file:
        path: "/etc/bashrc.d"
        state: directory

    - name: "Push bashrc parts"
      template:
        src: "etc/bashrc.d/{{ item }}.j2"
        dest: "/etc/bashrc.d/{{ item }}"
      loop:
        - prompt.sh
        - history.sh
        - options.sh
        - env.sh

    - name: "Make bashrc include bashrc.d"
      lineinfile:
        dest: "/etc/bash.bashrc"
        line: "for file in /etc/bashrc.d/*; do [ -r $file ] && source $file; done"

    - name: "Fix .bashrc for root"
      template:
        src: ".bashrc.j2"
        dest: "/root/.bashrc"

    - name: "Fix .bashrc in skel"
      template:
        src: ".bashrc.j2"
        dest: "/etc/skel/.bashrc"

    - name: "Update user .bashrc"
      template:
        src: ".bashrc.j2"
        dest: "/home/{{ my_user }}/.bashrc"
        owner: "{{ my_user }}"
        group: "{{ my_user }}"
        mode: 0640

    - name: "Git checkout the homedir-in-git repo"
      ansible.builtin.git:
        repo: "{{ my_repo }}"
        dest: "/home/{{ my_user }}"
        bare: "yes"

    - name: "Update user .gitconfig"
      template:
        src: ".gitconfig.j2"
        dest: "/home/{{ my_user }}/.gitconfig"
        owner: "{{ my_user }}"
        group: "{{ my_user }}"
        mode: 0640

    - name: "Add sudo rights for mkramdisk"
      template:
        src: "etc/sudoers.d/mkramdisk.j2"
        dest: "/etc/sudoers.d/mkramdisk"
        owner: "root"
        group: "root"
        mode: 0440
        validate: "visudo -cf %s"

    - name: "Add sudo rights for openvpn"
      template:
        src: "etc/sudoers.d/openvpn.j2"
        dest: "/etc/sudoers.d/openvpn"
        owner: "root"
        group: "root"
        mode: 0440
        validate: "visudo -cf %s"
